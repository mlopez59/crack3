#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    // TODO: FILL THIS IN
    char *ha;
    char *die;
};



// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    FILE *f;
    f = fopen(filename, "rb");
    char *dictionary = malloc(*size);
    fread(dictionary, 1, *size, f);
    dictionary[*size] = '\0';
    int diclines = 0;
    for (int i = 0; i < *size; i++)
    {
        if (dictionary[i] == '\n' ) diclines++;
    }
    //struct entry *dentry = malloc(diclines * ((PASS_LEN + HASH_LEN) * sizeof(struct entry)));
    struct entry *dentry = malloc(*size * sizeof(struct entry));
    char *line;
      for (int i = 0; i < diclines; i++)
    {
        if (i == 0)
        {
            line = strtok(dictionary, "\n");
        }
        else
        {
            line = strtok(NULL, "\n");
        }
        dentry[i].die = line;
    }
    for (int i = 0; i < diclines; i++)
    {
        dentry[i].ha = md5(dentry[i].die, strlen(dentry[i].die));
        //printf("%s___%s\n", dentry[i].die, dentry[i].ha);
    }
    //dentry[0].die = dicword;
    //dentry[0].ha = dicword;
    //printf("%s___\n", dentry[0].die);
    //printf("%s___\n", dentry[0].die);
    //free(dictionary);
    fclose(f); 
    return dentry;
}

int sortdict (const void *a, const void *b)
{
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    
    return strcmp((*aa).ha, (*bb).ha);
}
 
int hashsearch (const void *key, const void *elem)
{
    char *kk = (char *)key;
    struct entry *ee = (struct entry *)elem;
    return strcmp(kk, (*ee).ha);
}
 
int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    FILE *h;
    FILE *d;
    h = fopen(argv[1], "rb");
    if (!h)
    {
        printf("Could not open %s as a hash file for reading\n", argv[1]);
        exit(1);
    }
    d = fopen(argv[2], "rb");
    if (!d)
    {
        printf("Could not open %s as a dictionary file for reading\n", argv[2]);
        exit(1);
    }
    
    
    // TODO: Read the dictionary file into an array of entry structures
    struct stat dinfo;
    if (stat(argv[2], &dinfo) == -1)
    {
        printf("Can't stat %s\n", argv[2]);
        exit(1);
    }
    int dicsize = (dinfo.st_size + 1);
    struct entry *dict = read_dictionary(argv[2], &dicsize);
    //printf("%d\n", dicsize);
    //printf(" ***%s*** \n", (dict[50].die));
    
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    char *dlines = malloc(dicsize);
    fread(dlines, 1, dicsize, d);
    dlines[dicsize] = '\0';
    int dlinecount = 0;
    for (int i = 0; i < dicsize; i++)
    {
        if (dlines[i] == '\n' ) dlinecount++;
    }
    //printf("%d\n", dlinecount);
    qsort(dict, dlinecount, sizeof(struct entry), sortdict);
    /*
    for (int i = 0; i < dlinecount; i++)
    {
        //dentry[i].ha = md5(dentry[i].die, strlen(dentry[i].die));
        printf("%s___%s\n", dict[i].die, dict[i].ha);
    }
    */
    
    
    // TODO
    // Open the hash file for reading.
    int hlen = 0;
    struct stat hinfo;
    stat(argv[1], &hinfo);
    hlen = (hinfo.st_size + 1);
    char *hashd = malloc(hlen);
    fread(hashd, 1, hlen, h);
    hashd[hlen] = '\0';
    int hashlines = 0, k = 1;
    for (int i = 0; i < hlen; i++)
    {
        if (hashd[i] == '\n' ) hashlines++;
    }
    char ** hashdic = malloc(hashlines * sizeof(char *));
    hashdic[0] = strtok(hashd, "\n");
    while ((hashdic[k] = strtok(NULL, "\n")) != NULL)
    {
        k++; 
    }


    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    for (int u = 0; u < hashlines; u++)
    {
    struct entry *found = bsearch(hashdic[u], dict, dlinecount, sizeof(struct entry), hashsearch);
    if (found)
        {
            printf("%s____%s\n", (*found).die, (*found).ha);
        }
    }

    free(dlines);
    //free(hashdic[0]);
    free(hashd);
    free(dict[0].ha);
    //free(dict[0].die);
    free(dict);
    free(hashdic);
    fclose(h);
    fclose(d);
}
